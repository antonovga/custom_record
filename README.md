## Задача
Требуется реализовать функционал ActiveRecord,
необходим минимальный набор методов (where, create, update), под несколько баз данных (mysql, sqlite, postgresql).

### Версия ruby
`2.3.1`

### Системные зависимости
`sudo apt-get install libpq-dev libmysqlclient-dev`

### Подготовка

* установить gems `bundle install`
* создать необходимые базы
* в `./config/database.yml` указать необходимые параметры, минимум адаптер, имя базы, пользователь, (пароль)
* в прримерах видно, как используется `database.yml` при необходимости можно изменить/подправить.

### Примеры
`cd ./examples`

`ruby ./sqlite_example.rb`

`ruby ./mysql_example.rb`

`ruby ./postgresql_example.rb`

### Тесты
`ruby ./test/sqlite_test.rb`

`ruby ./test/mysql_test.rb`

`ruby ./test/postgresql_test.rb`

### Дополнительно

* не использовал `ActiveSupport` и прочие гемы т.к. обычно такие задания на голых руби предполагается выполнять
* не делал приведения типов, прилично писанины и не уверен, что требуется.
* просстенький парсер росфинмониторинга -> https://gitlab.com/antonovga/rosfinextreme
