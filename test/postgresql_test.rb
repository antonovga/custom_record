require 'minitest/autorun'
require 'pg'
require 'psych'
require_relative '../lib/custom_record'

DB_CONFIG_FILE = './config/postgresql_database.yml'

describe 'CustomRecord using mysql' do

  before do
    CustomRecord.db = CustomRecord::BaseAdapter.db_connection(DB_CONFIG_FILE)
    CustomRecord.db.execute('drop table if exists posts')
    CustomRecord.db.execute('create table if not exists posts (
    id     BIGSERIAL PRIMARY KEY,
    title  TEXT,
    content   TEXT
  )')

    class Post
      include CustomRecord::Mapper

      set_table_name :posts
    end
  end

  it 'must allow creating record' do
    count = Post.all.count
    Post.create title: 'Title', content: 'Content'
    Post.all.count.must_equal count+1
    Post.all.map{ |post| post.reject{ |k| k == :id } }.must_include({ 'id' => '1', 'title' => 'Title', 'content' => 'Content' })
  end

  it 'must allow updating record' do
    params = { title: 'Title', content: 'Content'}
    new_params = { title: 'New Title', content: 'New Content'}
    Post.create params
    count = Post.all.count
    Post.update 1, new_params
    Post.all.count.must_equal count
    CustomRecord.db.execute('select * from posts where id = 1' ).to_a.flatten.must_equal [{ 'id' => '1', 'title' => 'New Title', 'content' => 'New Content' }]
  end

  it 'must allow searching through table' do
    Post.create title: 'Title', content: 'Content'
    Post.create title: 'Title 2', content: 'Content'
    Post.create title: 'Title 3', content: 'Content'
    Post.where(title: 'Title 3').must_include({ 'id' => '3', 'title' => 'Title 3', 'content' => 'Content' })
    Post.where(title: 'Title 3').wont_include({ 'id' => '2', 'title' => 'Title 2', 'content' => 'Content' })
    Post.where(title: 'Title 3').wont_include({ 'id' => '1', 'title' => 'Title 1', 'content' => 'Content' })
  end
end
