require_relative './custom_record/mapper'
require_relative '../lib/custom_record/db_adapters/base_adapter'
require_relative '../lib/custom_record/db_adapters/sqlite_adapter'
require_relative '../lib/custom_record/db_adapters/mysql_adapter'
require_relative '../lib/custom_record/db_adapters/postgresql_adapter'

module CustomRecord
  class << self
    attr_accessor :db
  end
end