module CustomRecord
  class BaseAdapter
    attr_accessor :db

    def self.db_connection(config_file)
      raw = Psych.load_file(config_file)
      db_adapter = raw['db']['adapter']
      db_params = raw['db']['params']

      case db_adapter
        when 'sqlite'
          SqliteAdapter.new(db_params)
        when 'mysql'
          MysqlAdapter.new(db_params)
        when 'postgresql'
          PostgresqlAdapter.new(db_params)
        else
          raise 'wrong adapter param in config file'
      end
    end

    def execute(*raw_sql)
      raise NotImplementedError, 'Implement this method in child class'
    end

    def create(params)
      raise NotImplementedError, 'Implement this method in child class'
    end

    def update(params)
      raise NotImplementedError, 'Implement this method in child class'
    end

    def where(params)
      raise NotImplementedError, 'Implement this method in child class'
    end

    def all
      raise NotImplementedError, 'Implement this method in child class'
    end

    def get_table_info(table_name)
      raise NotImplementedError, 'Implement this method in child class'
    end

    protected

    def bind_vars(params, separator)
      params.keys.map { |k| "#{ k } = ?" }.join(separator)
    end

    def create_bind_vars(params)
      (['?'] * params.count).join(', ')
    end
  end
end