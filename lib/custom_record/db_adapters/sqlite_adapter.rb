module CustomRecord
  class SqliteAdapter < BaseAdapter
    def initialize(params)
      self.db = SQLite3::Database.new(params['database'])
    end

    def execute(*raw_sql)
      db.execute(*raw_sql)
    end

    def create(params, options)
      execute("insert into #{ options[:table_name] } (#{ params.keys.join(', ') }) values (#{ create_bind_vars(params) })",
              params.values)
    end

    def update(params, options)
      execute("update #{ options[:table_name] } set #{ bind_vars(params[:fields], ', ') } where #{ bind_vars(params[:where], ' AND ') }",
              params[:fields].values+params[:where].values)
    end

    def where(params, options)
      result = if params.is_a?(String)
                 execute("select * from #{ options[:table_name] } where #{ params }")
               else
                 execute("select * from #{ options[:table_name] } where #{ bind_vars(params, ' AND ') }",
                         params.values)
               end
      result.map { |row| Hash[options[:columns].zip(row)] }
    end

    def all(options)
      execute("select * from #{ options[:table_name] }").map { |row| Hash[options[:columns].zip(row)] }
    end

    def get_table_info(table_name)
      data = execute("PRAGMA table_info(#{ table_name })")
      primary_key, columns = nil, []
      data.each do |column|
        primary_key = column[1].to_sym if column[-1] == 1
        columns << column[1].to_sym
      end
      [primary_key, columns]
    end
  end
end