module CustomRecord
  class MysqlAdapter < BaseAdapter

    def initialize(params)
      self.db = Mysql2::Client.new(params)
    end

    def execute(*raw_sql)
      db.query(*raw_sql)
    end

    def create(params, options)
      query = db.prepare("insert into #{ options[:table_name]} (#{ params.keys.join(', ') }) values (#{ create_bind_vars(params) })")
      query.execute(*params.values)
    end

    def update(params, options)
      query = db.prepare("update #{ options[:table_name] } set #{ bind_vars(params[:fields], ', ') } where #{ bind_vars(params[:where], ' AND ') }")
      query.execute(*(params[:fields].values+params[:where].values))
    end

    def where(params, options)
      result = if params.is_a?(String)
                 execute("select * from #{ options[:table_name] } where #{ params }")
               else
                 query = db.prepare("select * from #{ options[:table_name] } where #{ bind_vars(params, ' AND ') }")
                 query.execute(*params.values)
               end
      result.to_a
    end

    def all(options)
      execute("select * from #{ options[:table_name] }").to_a
    end

    def get_table_info(table_name)
      data = execute("describe #{ table_name }")
      primary_key, columns = nil, []
      data.each do |column|
        primary_key = column['Field'].to_sym if column['Key'] == 'PRI'
        columns << column['Field'].to_sym
      end
      [primary_key, columns]
    end
  end
end