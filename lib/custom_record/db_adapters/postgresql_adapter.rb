module CustomRecord
  class PostgresqlAdapter < BaseAdapter

    def initialize(params)
      self.db = PG.connect(params)
    end

    def execute(*raw_sql)
      db.exec(*raw_sql)
    end

    def create(params, options)
      db.prepare('query', "insert into #{ options[:table_name]} (#{ params.keys.join(', ') }) values (#{ create_bind_vars(params) })")
      db.exec_prepared('query', params.values)
      db.exec('deallocate query')
    end

    def update(params, options)
      db.prepare('query', "update #{ options[:table_name] } set #{ bind_vars(params[:fields], ', ') } where #{ bind_vars(params[:where], ' AND ', params[:fields].count) }")
      db.exec_prepared('query', (params[:fields].values+params[:where].values))
      db.exec('deallocate query')
    end

    def where(params, options)
      result = []
      if params.is_a?(String)
        result = execute("select * from #{ options[:table_name] } where #{ params }")
      else
        db.prepare('query', "select * from #{ options[:table_name] } where #{ bind_vars(params, ' AND ') }")
        result = db.exec_prepared('query', params.values)
        db.exec('deallocate query')
      end
      result.to_a
    end

    def all(options)
      execute("select * from #{ options[:table_name] }").to_a
    end

    def get_table_info(table_name)
      data = execute("select column_name from information_schema.columns where table_name = '#{ table_name }'")
      columns = data.values.flatten.map(&:to_sym)
      primary_key = execute("select column_name from information_schema.key_column_usage join information_schema.table_constraints on key_column_usage.constraint_name = table_constraints.constraint_name where key_column_usage.table_name = '#{ table_name }' and constraint_type = 'PRIMARY KEY';").first['column_name'].to_sym

      [primary_key, columns]
    end

    private

    def bind_vars(params, separator, start_number = 0)
      params.keys.each_with_index.map{ |k, i| "#{ k } = $#{ i+1+start_number }" }.join(separator)
    end

    def create_bind_vars(params)
      1.upto(params.count).map{ |n| "$#{n}" }.join(', ')
    end
  end
end