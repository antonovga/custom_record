module CustomRecord
  module Mapper

    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods
      attr_accessor :table_name, :columns, :primary_key

      def set_table_name(table_name)
        self.table_name = table_name
        self.primary_key, self.columns = CustomRecord.db.get_table_info(table_name)
      end

      def create(params)
        raise unless params.keys.all? { |e| columns.include?(e) }
        CustomRecord.db.create(params, { table_name: table_name })
      end

      def update(id, params)
        raise unless params.keys.all? { |e| columns.include?(e) }
        CustomRecord.db.update({ where: { primary_key => id }, fields: params }, { table_name: table_name })
      end

      def where(params)
        raise unless params.is_a?(String) || params.keys.all? { |e| columns.include?(e) }
        CustomRecord.db.where(params, { table_name: table_name, columns: columns })
      end

      def all
        CustomRecord.db.all({ table_name: table_name, columns: columns })
      end
    end
  end
end