require 'sqlite3'
require 'psych'
require_relative '../lib/custom_record'

DB_CONFIG_FILE = '../config/database.yml'

CustomRecord.db = CustomRecord::BaseAdapter.db_connection(DB_CONFIG_FILE)
CustomRecord.db.execute('create table posts (
    id     INTEGER PRIMARY KEY,
    title  TEXT,
    content   TEXT
  )')

class Post
  include CustomRecord::Mapper

  set_table_name :posts
end

p 'ALL---------'
p Post.all
p 'CREATE---------'
Post.create(title: 'foo 1', content: 'bar 1')
p 'CREATE---------'
Post.create(title: 'foo 1', content: 'bar 2')
p 'CREATE---------'
Post.create(title: 'foo 3', content: 'bar 3')
p 'CREATE---------'
Post.create(title: 'foo 4', content: 'bar 4')
p 'ALL---------'
p Post.all
p 'WHERE[STRING]---------'
p Post.where(('title like "%1"'))
p 'WHERE[HASH]---------'
p Post.where(title: 'foo 3')
p 'UPDATE---------'
Post.update(1, title: 'updated title')
p 'ALL---------'
p Post.all